import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { LoginService } from '../Services/login.service';

@Injectable()
export class LoginGuard implements CanActivate {
  constructor(private loginService: LoginService, private router: Router) {}

  // This method is used to secure routes in the route module
  canActivate(): boolean {
    // If there is a token, proceed
    if(this.loginService.getIsAuthenticated()){
      return true;
    }else{
      // Or else redirect to the login screen
      this.router.navigate(['/login']);
      return false;
    }
  }
}