/*
    inputField.ts - Input Field Object
    ---------------------
    The input field object is used for adding input fields to a form object.

    [00] label - label added on the input field
    [01] inputType - the type of input field
    [02] type - input type text/number
    [03] placeholder - the placeholder added to the input type
    [04] fieldInput - used for input field options that may be included with check box/radio button/selection box
*/


export class InputField{
    label: string;
    inputType: string;
    type: string;
    placeholder: string;
    fieldInput: any;
}