/*
    user.ts - User Object
    ---------------------
    The user object is a replica of the object saved on the backend.
    This object is used for operating CRUD operations on the user DB.

    [00] userId
    [01] firstName
    [02] surname
    [03] username
    [04] email
    [05] password
    [06] companyId
    [07] dateCreated
    [08] dateUpdated
    [09] lastLogin
    [10] createdBy
*/

export class User{
    _id: number;
    _rev: any;
    firstName: string;
    surname: string;
    username: string;
    email: string;
    privileges: string;
    password: string;
    companyId: number;
    dateCreated: Date;
    dateUpdated: Date;
    lastLogin: Date;
    createdBy: number;
}