/*
    form.ts - Form Object
    ---------------------
    The form object is a replica of the object saved on the backend.
    This object is used for operating CRUD operations on the form DB.

    [00] label - name of the form
    [01] createdBy - user that created the form
    [02] company - company the user works for
    [03] _id
    [04] _rev
    [05] dateCreated - date the form was created
    [06] dateUpdated - date the form was updated
    [07] content - array of InputField objects for rendering the form
    [08] dynamicObject - form built for submitting data
*/


import { InputField } from './inputField';

export class Form{

    label: string;
    createdBy: number;
    company: number;
    _id: string;
    _rev: string;
    dateCreated: Date;
    dateUpdated: Date;
    content: InputField[];
    dynamicObject: any;
}