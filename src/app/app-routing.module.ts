/* This takes care of all the routing. */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './Components/pages/home/home.component';
import { NavbarComponent } from './Components/pages/navbar/navbar.component';
import { AddUserComponent } from './Components/pages/users/add-user/add-user.component';
import { EditUserComponent } from './Components/pages/users/edit-user/edit-user.component';
import { ManageUsersComponent } from './Components/pages/users/manage-users/manage-users.component';
import { CreateFormComponent } from './Components/pages/forms/create-form/create-form.component';
import { ManageFormsComponent } from './Components/pages/forms/manage-forms/manage-forms.component';
import { FormDetailsComponent } from './Components/pages/forms/form-details/form-details.component';

import { LoginGuard } from './Guards/login.guard';

// This is the object array of routes, when the browser window follows one of the paths it redirects the router-outlet
// to display the html file associated with that path's component.
const routes: Routes = [
  // Default route
  { path: '', redirectTo: '/manage-users', pathMatch: 'full' },
  { path: 'login', component: HomeComponent},
  { path: 'navbar', component: NavbarComponent },
  { path: 'add-user', component: AddUserComponent, canActivate: [LoginGuard]},
  { path: 'manage-users', component: ManageUsersComponent, canActivate: [LoginGuard] },
  { path: 'create-form', component: CreateFormComponent, canActivate: [LoginGuard] },
  { path: 'manage-forms', component: ManageFormsComponent, canActivate: [LoginGuard] },
  { path: 'form-details/:_id', component: FormDetailsComponent, canActivate: [LoginGuard]},
  { path: 'edit-user/:_id', component: EditUserComponent, canActivate: [LoginGuard] },
  // Default route if the route does not match.
  { path: '**', redirectTo: '/manage-users'}
];

@NgModule({
  imports: [
    CommonModule,
     RouterModule.forRoot(routes),
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
