
/* This is basically the heart of the angular application. every other component relies on this one. 
  This is where all the dependancy injections are made etc. */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './Components/pages/home/home.component';
import { AppRoutingModule } from './/app-routing.module';
import { CoreModule } from './Components/core/core.module';
// import { SharedModule } from './Components/shared/shared.module';
import { NavbarComponent } from './Components/pages/navbar/navbar.component';
import { AddUserComponent } from './Components/pages/users/add-user/add-user.component';
import { ManageUsersComponent } from './Components/pages/users/manage-users/manage-users.component';
import { CreateFormComponent } from './Components/pages/forms/create-form/create-form.component';
import { ManageFormsComponent } from './Components/pages/forms/manage-forms/manage-forms.component';
// import { ManageScheduleComponent } from './Components/pages/manage-schedule/manage-schedule.component';
import { InputBoxComponent } from './Components/pages/forms/form-fields/input-box/input-box.component';
import { TextAreaComponent } from './Components/pages/forms/form-fields/text-area/text-area.component';
import { DatePickerComponent } from './Components/pages/forms/form-fields/date-picker/date-picker.component';
import { CheckBoxComponent } from './Components/pages/forms/form-fields/check-box/check-box.component';
import { RadioButtonComponent } from './Components/pages/forms/form-fields/radio-button/radio-button.component';
import { DropdownBoxComponent } from './Components/pages/forms/form-fields/dropdown-box/dropdown-box.component';
import { EditUserComponent } from './Components/pages/users/edit-user/edit-user.component';
import { FormDetailsComponent } from './Components/pages/forms/form-details/form-details.component';

import { APIService } from './Services/api.service';
import { CookieService } from 'ngx-cookie-service';
import { LoginService } from './Services/login.service';

import { LoginGuard } from './Guards/login.guard';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    AddUserComponent,
    ManageUsersComponent,
    CreateFormComponent,
    ManageFormsComponent,
    // ManageScheduleComponent,
    InputBoxComponent,
    TextAreaComponent,
    DatePickerComponent,
    CheckBoxComponent,
    RadioButtonComponent,
    DropdownBoxComponent,
    EditUserComponent,
    FormDetailsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    FormsModule,
    // SharedModule,
    HttpModule,
    JsonpModule,
    ReactiveFormsModule
  ],
  providers: [
    APIService,
    LoginService,
    CookieService,
    LoginGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
