import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';


@Injectable()
export class LoginService {

  constructor(private cookieService: CookieService) { }
  
  setToken(cookie: any): void{
    // Set a cookie
    this.cookieService.set('user', cookie, (60/1440));
  }

  getToken(): any{
    // Return a cookie
    return this.cookieService.get('user');
  }

  deleteToken(): void{
    // Delete a cookie
    this.cookieService.delete('user');
  }

  // Checks if the there is a cookie and returns a boolean
  getIsAuthenticated(): boolean{
    var tokenValid = this.getToken();
    try{ 
      tokenValid = JSON.parse(tokenValid);
      if(tokenValid.token != undefined || tokenValid.token != null){
        return true;
      }else{
       return false;
      }
    }catch(error){
      console.log("Does not exist");
      return false;
    }
    
    
  }
}
