import { Injectable }     from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { User } from '../Models/user';
import { Form } from '../Models/form';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class APIService{

    constructor (private http: Http) {}

    // The pre-fix to any api call
    private sareformAPI = 'http://localhost:4201/api/';

    // Authentication routes
    authenticateUser(loginCreds): Observable<User> {
        return this.http.post(this.sareformAPI+"authenticate", loginCreds)
                        .map((res:Response) => res.json())
                        .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
    }

    // User routes
    postUser(newUser: User): Observable<String> {
        return this.http.post(this.sareformAPI+"users", newUser)
                        .map((res:Response) => res.json())
                        .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
    }

    getUsers(): Observable<User> {
        return this.http.get(this.sareformAPI+"users")
                        .map((res:Response) => res.json())
                        .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
    }

    getUser(userId): Observable<User> {
        return this.http.get(this.sareformAPI+"users/"+userId)
                        .map((res:Response) => res.json())
                        .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
    }

    putUser(updatedUser: User): Observable<String> {
        console.log("users/" + updatedUser._id);
        return this.http.put(this.sareformAPI+"users/"+updatedUser._id, updatedUser)
                        .map((res:Response) => res.json())
                        .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
    }

    deleteUser(userIds: Number[]): Observable<String>{
        return this.http.delete(this.sareformAPI+"users/"+JSON.stringify(userIds))
                        .map((res:Response) => res.json())
                        .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
    }

    // Form routes
    
    postForm(newForm: Form): Observable<String> {
        return this.http.post(this.sareformAPI+"forms", newForm)
                        .map((res:Response) => res.json())
                        .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
    }

    getForms(): Observable<Form> {
        return this.http.get(this.sareformAPI+"forms")
                        .map((res:Response) => res.json())
                        .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
    }

    getForm(formId): Observable<Form> {
        return this.http.get(this.sareformAPI+"forms/"+formId)
                        .map((res:Response) => res.json())
                        .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
    }

    putForm(updatedForm: Form): Observable<String> {
        console.log("forms/" + updatedForm._id);
        return this.http.put(this.sareformAPI+"forms/"+updatedForm._id, updatedForm)
                        .map((res:Response) => res.json())
                        .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
    }

    deleteForm(formId: String[]): Observable<String>{
        return this.http.delete(this.sareformAPI+"forms/"+JSON.stringify(formId))
                        .map((res:Response) => res.json())
                        .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
    }
}