import { Component, OnInit } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { Router, ActivatedRoute } from '@angular/router';
import { APIService } from '../../../Services/api.service';
import { LoginService } from '../../../Services/login.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private apiservice: APIService,
    private loginservice: LoginService) { }

  loginCreds: any = {
    username: null,
    password: null
  };

  messageFromServer: boolean;
  message: string;

  // Method to login, if successful it wil print a message, create a token and redirect to default route, 
  // else it will print error message
  login(): void {
    if (this.loginCreds.username != null && this.loginCreds.password != null) {
      var user: any = null;
      this.apiservice.authenticateUser(this.loginCreds).subscribe(
        (response: any) => {
          user = response;
          this.messageFromServer = true;
          if (user.success) {
            this.loginservice.setToken( JSON.stringify({ user: user.user, token: user.token }));
            this.message = "Welcome " + user.user.username;
            setTimeout(() => {
              window.location.reload();
              this.router.navigate(['/manage-users']);
            }, 500);
          }else{
            this.messageFromServer = true;
            this.message = response.message;
          }
          console.log(user);
        },
        (err: any) => {
          console.log(err);
        });
    }
  }

  ngOnInit(): void {
    this.loginservice.deleteToken();
    this.messageFromServer = false;
    this.message = "";
    if(this.loginservice.getIsAuthenticated){
      this.router.navigate(['/manage-users']);
    }
  }
}
