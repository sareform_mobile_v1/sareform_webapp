import { Component, OnInit } from '@angular/core';

import { APIService } from '../../../../Services/api.service';
import { User } from '../../../../Models/user';
import { LoginService } from '../../../../Services/login.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(private apiservice: APIService, private loginservice: LoginService) { }

  messageFromServer: boolean;
  message: string;
  success: boolean;
  currentDate: Date = new Date();
  newUser: User = {
    _id: undefined,
    _rev: undefined,
    firstName: undefined,
    surname: undefined,
    username: undefined,
    email: undefined,
    privileges: undefined,
    password: undefined,
    companyId: undefined,
    dateCreated: undefined,
    dateUpdated: undefined,
    lastLogin: undefined,
    createdBy: undefined,
  };

  // Resets the filled out form
  continueAdding(){
    this.newUser.firstName = undefined;
    this.newUser.surname = undefined;
    this.newUser.email = undefined;
    this.newUser.privileges = undefined;
    this.newUser.username = undefined;
    this.newUser.password = undefined;
    this.messageFromServer = false;
  }

  // Resets message and lets you edit the employee
  editEmployee(){
    this.messageFromServer = false;
    this.message = null;
  }

  // Makes an api call to add the user and waits for a server message
  addUser() {
    this.apiservice.postUser(this.newUser)
      .subscribe(
      (response: any) => {
        this.messageFromServer = true;
        this.message = response.message;
        if(response.success){
          this.success = true;
        }else{
          this.success = false;
        }
        
      },
      (err: any) => {
        console.log(err);
      });
  }

  ngOnInit() {
    this.messageFromServer = false;
    this.newUser.companyId = 1;
    this.newUser.dateCreated = this.currentDate;
    this.newUser.dateUpdated = this.currentDate;
    this.newUser.lastLogin = undefined;
    this.newUser.createdBy = JSON.parse(this.loginservice.getToken()).user._id;
  }
}



