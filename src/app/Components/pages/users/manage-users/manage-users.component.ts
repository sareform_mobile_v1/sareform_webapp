import { Component, OnInit } from '@angular/core';

import { APIService } from '../../../../Services/api.service';
import { User } from '../../../../Models/user';
import { forEach } from '@angular/router/src/utils/collection';


@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.css']
})
export class ManageUsersComponent implements OnInit {

  constructor(private apiservice: APIService) { }
  zeroEmployees: boolean = true;
  addEmployee: boolean = false;
  showDetails: boolean = false;
  employeeButtonTooltips: string[];
  employeeToolbarConent: any[][] = [[]];
  employees: User[];
  employeeDetails: User;
  selectMode: boolean;
  selected: boolean[];
  selectedIndices: number[];
  selectedItems: User[];
  selectedCount: number;

  addFirstEmployee_Click(): void {
    this.addEmployee = true;
  }

  // UI feature to show the full name of the tooltip when mouse over
  expandToolTips(employeeIndex: number, tooltipIndex: number): void {
    this.employeeToolbarConent[employeeIndex][tooltipIndex] = this.employeeButtonTooltips[tooltipIndex];
  }

  // Sets all the tooltips to icons again
  collapseTooltips(): void {
    for (let i = 0; i < this.employees.length; i++) {
      this.employeeToolbarConent[i] = ['<i class="fa fa-expand"></i>', '<i class="fa fa-pencil"></i>', '<i class="fa fa-trash-o"></i>'];
    }
  }

  // Opens the side bar with user details
  openDetails(employee: User):void{
    this.employeeDetails = employee;
    this.showDetails = true;
  }

  // Closes the side bar with user details
  closeDetails():void{
    this.employeeDetails = null;
    this.showDetails = false;
  }

  // Retrieves all the users from the databse and displays them on the view
  getUsers(): void {
    this.apiservice.getUsers()
      .subscribe(
      (response: any) => {
        this.employees = response;
        this.collapseTooltips();
        this.employees.length < 1 ? this.zeroEmployees = true: this.zeroEmployees = false;
        this.sortEmployees();
      },
      (err: any) => {
        console.log(err);
      });
  }

  // Sorts the employees based on privileges
  sortEmployees(): void{
    this.employees = this.employees.sort((a,b)=> 0 - (a.privileges == "Administrator" ? 1 : -1));
  }

  // Deletes one user from the database and updates the list
  deleteUser(user: User): void{
    if(this.deleteWarning()){
      var userIds: any[] = [{_id: user._id, _rev: user._rev, _deleted: true}];
      this.apiservice.deleteUser(userIds)
      .subscribe(
      (response: any) => {
        for(let employee of this.employees){
          console.log(employee._id + " " + user._id);
          if(employee._id == user._id){
            let index = this.employees.indexOf(employee, 0);
            if (index > -1) {
              this.employees.splice(index, 1);
            }
          }
        }
        this.sortEmployees();
      },
      (err: any) => {
        console.log(err);
      });
    }else{
      return;
    }
  }

  // Confirmation before deleting
  deleteWarning(): Boolean{
    return this.selectedCount < 1 ? 
    confirm("Are you sure you want to delete this employee?") : 
    confirm("Are you sure you want to delete " + this.selectedCount + " employees?");
  }
  
  // Deletes multiple users - works to delete them but the view doesn't always show changes.
  deleteUsers(): void{
    if(this.deleteWarning()){
      var userIds: any[] = [];

      for(let user of this.selectedItems){
        userIds.push({_id: user._id, _rev: user._rev, _deleted: true});
      }
      this.apiservice.deleteUser(userIds)
      .subscribe(
      (response: any) => {
        for(var i = this.selectedIndices.length -1; i >= 0; i--){
          this.employees.splice(this.selectedIndices[i],1);
        }
        this.resetSelect();
        this.sortEmployees();
      },
      (err: any) => {
        console.log(err);
      });
    }else{
      return;
    }
  } 

  // Toggles the select mode
  toggleSelectMode(){
    this.showDetails = false;
    this.selectMode = !this.selectMode;
    this.resetSelect();
  }
  
  // Resets all the selected items
  resetSelect(): void{
    this.selectedCount = 0;
    for(var i = 0; i < this.employees.length; i++){
      this.selected[i] = false;
    }
    this.selectedIndices = [];
    this.selectedItems = [];
  }

  // UI feature to select several items and logic that if clicked again it is no longer selected etc.
  selectItem(index: number){
    if(this.selectMode){
      if(this.selectedItems.length > 0 && this.selectedItems.includes(this.employees[index])){
        this.selectMode ? this.selectedCount -= 1 : this.selectedCount = this.selectedCount;
        var empIndex = this.selectedItems.indexOf(this.employees[index], 0);
        if (empIndex > -1) {
          this.selectedItems.splice(empIndex, 1);
          this.selectedIndices.splice(index, 1);
          this.selected[index] = false;
        }
      }else{
        this.selectMode ? this.selectedCount += 1 : this.selectedCount = this.selectedCount;
        this.selectedItems.push(this.employees[index]);
        this.selectedIndices.push(index);
        this.selected[index] = true;
      }
    }
  }

  ngOnInit() {
    this.employeeButtonTooltips = ["Details", "Edit", "Delete"];
    this.selected = [];
    this.selectedItems = [];
    this.selectedIndices = [];
    this.selectMode = false;
    this.selectedCount = 0;
    // On load get the users
    this.getUsers();
  }
}
