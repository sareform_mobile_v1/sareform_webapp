import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { APIService } from '../../../../Services/api.service';
import { User } from '../../../../Models/user';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  messageFromServer: boolean;
  message: string;
  id: number;
  private sub: any;
  thisUser: User;
  constructor(private route: ActivatedRoute,private apiservice: APIService, private router: Router) { }

  // Retrieves the user from database through api call
  getUser(id): void {
    this.apiservice.getUser(id)
      .subscribe(
      (response: any) => {
        this.thisUser = response;
      },
      (err: any) => {
        console.log(err);
      });
  }

  // Makes a put api call with the edited user data
  editUser() {
    this.thisUser.dateUpdated = new Date();
    this.apiservice.putUser(this.thisUser)
      .subscribe(
      (response: any) => {
        this.messageFromServer = true;
        this.message = response.message;
        setTimeout(() => {
          this.router.navigate(['/manage-users']);
        }, 50);
      },
      (err: any) => {
        console.log(err);
      });
  }

  ngOnInit() {
    this.messageFromServer = false;
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['_id']; // (+) converts string 'id' to a number
      this.getUser(this.id);
      // In a real app: dispatch action to load the details here.
   });
  }

}
