/*
    navbar.ts - Navbar Component
    ---------------------
    Each Page and Component will be wrapped inside of the Navbar which sits at the top of the page.
    The navbar will navigate the user to different pages.
    
    (1) Imports the Router for navigating as 'router' for reference.
    (2) Navigation function that takes in a parameter that will navigate the user to different
    pages based on the parameter fed into the function.
    (3) When the page is initialized or loaded, the buttons for navigating are made for templating.    
*/

import { Component, OnInit } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { Router } from '@angular/router';
import { LoginService } from '../../../Services/login.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})


export class NavbarComponent implements OnInit {

  navMenu: any;

  // (1)
  constructor(private router: Router,
              private loginservice: LoginService) { }

  logout(): void{
    this.loginservice.deleteToken();
    setTimeout(() => {
      window.location.reload();
    }, 50);
  }

  // (2)
  navigatePage(paremeter): void {

    if (paremeter == "create-form") {
      setTimeout(() => {
        this.router.navigate(['/create-form']);
      }, 50);
    }

    else if (paremeter == "manage-forms") {
      setTimeout(() => {
        this.router.navigate(['/manage-forms']);
      }, 50);
    }


    else if (paremeter == "create-employee") {
      setTimeout(() => {
        this.router.navigate(['/add-user']);
      }, 50);
    }

    else if (paremeter == "manage-employees") {
      setTimeout(() => {
        this.router.navigate(['/manage-users']);
      }, 50);
    }
  }

  // (3)
  ngOnInit() {

    this.navMenu = [

      {
        title: "Employees",

        menuItems: [
          {
            title: "Manage Employees",
            id: "manage-employees"
          },
          {
            title: "Add Employee",
            id: "create-employee"
          },
        ]

      },

      {
        title: "Forms",

        menuItems: [
          {

            title: "Manage Forms",
            id: "manage-forms"
          },
          {
            title: "Create Form",
            id: "create-form"
          }
        ]
      }
    ]
  }

}
