/*
    form-details.component.ts - Form Details Component
    ---------------------
    The 'Form Details' Component is used for getting the details about a single form. This component is not used
    in the application.

    (1) Does a get request for a single form using the form ID and sets the 'dynamicForm'
    to the response
*/
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { APIService } from '../../../../Services/api.service';
import { Form } from '../../../../Models/form';

@Component({
  selector: 'app-form-details',
  templateUrl: './form-details.component.html',
  styleUrls: ['./form-details.component.css']
})
export class FormDetailsComponent implements OnInit {
  dynamic_form : Form;

  constructor(private route: ActivatedRoute,private apiservice: APIService, private router: Router) { }

  // (1)
  getForm(id): void {
    this.apiservice.getForm(id)
      .subscribe(
      (response: any) => {
        this.dynamic_form = response;
      },
      (err: any) => {
        console.log(err);
      });
  }

  ngOnInit() {
  }

}
