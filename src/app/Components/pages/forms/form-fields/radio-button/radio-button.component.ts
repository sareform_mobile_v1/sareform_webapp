/*
    radio-button.components.ts - Radio Button Component
    ---------------------
    The Radio Button Component is the view that will be displayed when adding a
    'Radio Button' is selected for adding to the form.

    The functions are very very similar to the checkbox use those as reference!
*/

import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { InputField } from '../../../../../Models/inputField';

@Component({
  selector: 'app-radio-button',
  templateUrl: './radio-button.component.html',
  styleUrls: ['./radio-button.component.css']
})
export class RadioButtonComponent implements OnInit {

  @Output() inputObject: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  newInputField: InputField = {
    label: undefined,
    inputType: undefined,
    type: undefined,
    placeholder: undefined,
    fieldInput: undefined
  };

  form;
  radioButtonOptions = [];
  option: any;
  editedOption: any;
  editOn: boolean[];
  editableOptions = [];

  ngOnInit() {
    this.form = new FormGroup({
      
      label: new FormControl("", Validators.required),
      option: new FormControl("")
    });

    this.editOn = [];
  }

  newField = function(label){
    this.newInputField.label = this.form.value.label;
    this.newInputField.inputType = "radio button";
    this.newInputField.type = null;
    this.newInputField.placeholder = null;
    this.newInputField.fieldInput = this.radioButtonOptions;

    this.inputObject.emit(this.newInputField);
  }

  addOption(param): void {  
    this.radioButtonOptions.push(param);
    this.option = null;
    this.editableOptions.push(0);
    this.editOn.push(false);
  }

  deleteOption(param): void{
    this.radioButtonOptions.splice(param, 1);
    this.editableOptions.splice(param, 1);
  }

  editOption(index, param): void{
    this.editOn[index] = true;
    this.editedOption = param;
  }

  updateOption(index): void{
    this.editOn[index] = false;
  }

  trackByFn(index: any, item: any) {
    return index;
 }

}
