/*
    dropdown-box.components.ts - Dropdown Box Component
    ---------------------
    The Dropdown Box Component is the view that will be displayed when adding a
    'Dropdown Box' is selected for adding to the form.

    The functions are very very similar to the checkbox use those as reference!
*/

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { InputField } from '../../../../../Models/inputField';

@Component({
  selector: 'app-dropdown-box',
  templateUrl: './dropdown-box.component.html',
  styleUrls: ['./dropdown-box.component.css']
})
export class DropdownBoxComponent implements OnInit {

  @Output() inputObject: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  form;
  dropdownBoxOptions = [];
  option: any;
  editedOption: any;
  editOn: boolean[];
  editableOptions = [];
  
  newInputField: InputField = {
    label: undefined,
    inputType: undefined,
    type: undefined,
    placeholder: undefined,
    fieldInput: undefined
  };

  ngOnInit() {
    this.form = new FormGroup({
      
      label: new FormControl("", Validators.required),
      option: new FormControl("")
    });
    
    this.editOn = [];
  }

  newField = function(){
    this.newInputField.label = this.form.value.label;
    this.newInputField.inputType = "dropdown box";
    this.newInputField.type = null;
    this.newInputField.placeholder = null;
    this.newInputField.fieldInput = this.dropdownBoxOptions;

    this.inputObject.emit(this.newInputField);
  }

  addOption(param): void {  
    this.dropdownBoxOptions.push(param);
    this.option = null;
    this.editableOptions.push(0);
    this.editOn.push(false);
  }

  deleteOption(param): void{
    this.dropdownBoxOptions.splice(param, 1);
    this.editableOptions.splice(param, 1);
  }

  editOption(index, param): void{
    this.editOn[index] = true;
    this.editedOption = param;
  }

  updateOption(index): void{
    this.editOn[index] = false;
  }

  trackByFn(index: any, item: any) {
    return index;
 }

}
