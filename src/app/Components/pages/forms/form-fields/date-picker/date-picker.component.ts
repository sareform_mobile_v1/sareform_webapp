/*
    date-picker.components.ts - Date Picker Component
    ---------------------
    The Date Picker Component is the view that will be displayed when adding a
    'Date Picker' is selected for adding to the form.

    (1) Binds an EventEmitter to this component making it a child component
    of the parent 'Date Picker' component and handles events. This is done so
    '$onEvent' this will be pushed into the parent.
    (2) Initialize an instance of the InputField object to be filled out.
    (3) When the page is initialized or loaded, this function is called and
    will add validators to the form to ensure a label is added.
    (4) In this function the newInputField object is filled out and and is
    pushed up to the Form parent.
*/

import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { InputField } from '../../../../../Models/inputField';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.css']
})
export class DatePickerComponent implements OnInit {
  // (1)
  @Output() inputObject: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }
  // (2)
  newInputField: InputField = {
    label: undefined,
    inputType: undefined,
    type: undefined,
    placeholder: undefined,
    fieldInput: undefined
  };

  form;
  field: any;

  // (3)
  ngOnInit() {
    this.form  = new FormGroup({
      label : new FormControl("", Validators.required)
    });

  }

  // (4)
  newField = function(label){
    this.newInputField.label = this.form.value.label;
    this.newInputField.inputType = "date picker";
    this.newInputField.type = null;
    this.newInputField.placeholder = null;
    this.newInputField.fieldInput = null;
    
    this.inputObject.emit(this.newInputField);
  }

}
