/*
    input-box.components.ts - Input Box Component
    ---------------------
    The Input Box Component is the view that will be displayed when adding a
    'Input Box' is selected for adding to the form.

    (1) Binds an EventEmitter to this component making it a child component
    of the parent 'Create Form' component and handles events. This is done so
    '$onEvent' this will be pushed into the parent.
    (2) Initialize an instance of the InputField object to be filled out.
    (3) When the page is initialized or loaded, this function is called and
    will add validators to the form to ensure a label is added. It will also create
    button objects which will be templated out.
    (4) In this function the newInputField object is filled out and and is
    pushed up to the Form parent.
*/

import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { InputField } from '../../../../../Models/inputField';

@Component({
  selector: 'app-input-box',
  templateUrl: './input-box.component.html',
  styleUrls: ['./input-box.component.css']
})


export class InputBoxComponent implements OnInit {

  // (1)
  @Output() inputObject: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  // (2)
  newInputField: InputField = {
    label: undefined,
    inputType: undefined,
    type: undefined,
    placeholder: undefined,
    fieldInput: undefined
  };

  form;
  buttons: any;
  field: any;
  input: string;


  // (3)
  ngOnInit() {

    this.buttons = [
      {
        name: "Text",
        icon: "fa fa-font fa-5x",
        id: "text"
      },
      {
        name: "Numbers",
        icon: "fa fa-sort-numeric-desc fa-5x",
        id: "numbers"
      }
    ]

    this.form = new FormGroup({

      label: new FormControl("", Validators.required),
      type: new FormControl("", Validators.required),
      placeholder: new FormControl("")
    });

    this.form.controls['type'].setValue('text', { onlySelf: true });
  }

  // (4)
  newField = function () {
    this.newInputField.label = this.form.value.label;
    this.newInputField.inputType = "input box";
    this.newInputField.type = this.form.value.type;
    this.newInputField.placeholder = this.form.value.placeholder;
    this.newInputField.fieldInput = null;

    this.inputObject.emit(this.newInputField);

  }

}
