/*
    check-box.components.ts - Check Box Component
    ---------------------
    The Check Box Component is the view that will be displayed when adding a
    'Check Box' is selected for adding to the form.

    (1) Binds an EventEmitter to this component making it a child component
    of the parent 'Create Form' component and handles events. This is done so
    '$onEvent' this will be pushed into the parent.
    (2) Initialize an instance of the InputField object to be filled out.
    (3) When the page is initialized or loaded, this function is called and
    will add validators to the form to ensure a label is added.
    (4) In this function the newInputField object is filled out and and is
    pushed up to the Form parent.
    (5) Adds an option to the check box options
    (6) Allows for the deleting of the option by removing the option
    at the selected index.
    (7) Allows for the editing of an option at the selected index by enabling
    edit
    (8) Updates the option at the selected index
*/

import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { InputField } from '../../../../../Models/inputField';

@Component({
  selector: 'app-check-box',
  templateUrl: './check-box.component.html',
  styleUrls: ['./check-box.component.css']
})
export class CheckBoxComponent implements OnInit {

  // (1)
  @Output() inputObject: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }


  // (2)
  newInputField: InputField = {
    label: undefined,
    inputType: undefined,
    type: undefined,
    placeholder: undefined,
    fieldInput: undefined
  };

  form;
  checkBoxOptions = [];
  option: any;
  editedOption: any;
  editOn: boolean[];
  editableOptions = [];

  // (3)
  ngOnInit() {
    this.form = new FormGroup({
      label: new FormControl("", Validators.required),
      option: new FormControl("")
    });

    this.editOn = [];
  }

  // (4)
  newField(): void{
    this.newInputField.label = this.form.value.label;
    this.newInputField.inputType = "check box";
    this.newInputField.type = null;
    this.newInputField.placeholder = null;
    this.newInputField.fieldInput = this.checkBoxOptions;

    this.inputObject.emit(this.newInputField);
  }

  // (5)
  addOption(param): void {  
    this.checkBoxOptions.push(param);
    this.option = null;
    this.editableOptions.push(0);
    this.editOn.push(false);
  }

  // (6)
  deleteOption(param): void{
    this.checkBoxOptions.splice(param, 1);
    this.editableOptions.splice(param, 1);
  }

  // (7)
  editOption(index, param): void{
    this.editOn[index] = true;
    this.editedOption = param;
  }

  // (8)
  updateOption(index): void{
    this.editOn[index] = false;
  }

  trackByFn(index: any, item: any) {
    return index;
 }
}
