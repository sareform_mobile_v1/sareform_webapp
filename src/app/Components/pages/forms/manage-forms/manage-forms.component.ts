import { Component, OnInit } from '@angular/core';
import { APIService } from '../../../../Services/api.service';
import { Form } from '../../../../Models/form';

@Component({
  selector: 'app-manage-forms',
  templateUrl: './manage-forms.component.html',
  styleUrls: ['./manage-forms.component.css']
})
export class ManageFormsComponent implements OnInit {

  constructor(private apiservice: APIService) { }
  
  // Member variables
  zeroForms: boolean = true;
  addForm: boolean = false;
  showDetails: boolean = false;
  formButtonTooltips: string[];
  formToolbarConent: any[][] = [[]];
  forms: Form[];
  formDetails: Form;
  selectMode: boolean;
  selected: boolean[];
  selectedItems: Form[];
  selectedIndices: number[];
  selectedCount: number;
  
  // Button to show hidden elements in html
  addFirstForm_Click(): void {
    this.addForm = true;
  }

  // UI feature to show the full name of the tooltip when mouse over
  expandToolTips(formIndex: number, tooltipIndex: number): void {
    this.formToolbarConent[formIndex][tooltipIndex] = this.formButtonTooltips[tooltipIndex];
  }

  // Sets all the tooltips to icons again
  collapseTooltips(): void {
    for (let i = 0; i < this.forms.length; i++) {
      this.formToolbarConent[i] = ['<i class="fa fa-expand"></i>', '<i class="fa fa-pencil"></i>', '<i class="fa fa-trash-o"></i>', '<i class="fa fa-list"></i>'];
    }
  }

  // openDetails(form: Form): void {
  //   this.formDetails = form;
  //   this.showDetails = true;
  // }

  // closeDetails(): void {
  //   this.formDetails = null;
  //   this.showDetails = false;
  // }

  // Funtion to delete a form
  deleteForm(form: Form): void{
    if(this.deleteWarning()){
      console.log(form);
      var formIds: any[] = [{_id: form._id, _rev: form._rev, _deleted: true}];
      this.apiservice.deleteForm(formIds)
      .subscribe(
      (response: any) => {
        for(let f of this.forms){
          console.log(f._id + " " + form._id);
          if(f._id == form._id){
            let index = this.forms.indexOf(f, 0);
            if (index > -1) {
              this.forms.splice(index, 1);
            }
          }
        }
        this.sortForms();
      },
      (err: any) => {
        console.log(err);
      });
    }else{
      return;
    }
  }

  // Confirm popup
  deleteWarning(): Boolean{
    return this.selectedCount < 1 ? 
    confirm("Are you sure you want to delete this form?") : 
    confirm("Are you sure you want to delete " + this.selectedCount + " forms?");
  }

  // Function to delete multiple forms - this one does not always work.
  deleteForms(): void{
    if(this.deleteWarning()){
      var formIds: any[] = [];
      console.log(this.selectedItems);
      for(let f of this.selectedItems){
        formIds.push({_id: f._id, _rev: f._rev, _deleted: true});
      }
      this.apiservice.deleteUser(formIds)
      .subscribe(
      (response: any) => {
        for(var i = this.selectedIndices.length -1; i >= 0; i--){
          this.forms.splice(this.selectedIndices[i],1);
        }
        this.resetSelect();
        this.sortForms();
      },
      (err: any) => {
        console.log(err);
      });
    }else{
      return;
    }
  }

  // Get forms from database
  getForms(): void {
    this.apiservice.getForms()
      .subscribe(
        (response: any) => {
          this.forms = response;
          this.collapseTooltips();
          this.forms.length < 1 ? this.zeroForms = true : this.zeroForms = false;
          console.log(this.forms);
          this.sortForms();
        },
        (err: any) => {
          console.log(err);
        });
  }

  // Sort forms by datecreated
  sortForms(): void{
    this.forms = this.forms.sort((a,b)=> 0 - (a.dateCreated < b.dateCreated ? 1 : -1));
  }

  // UI feature to toggle select mode
  toggleSelectMode() {
    this.selectMode = !this.selectMode;
    this.resetSelect();
  }

  // UI feature to reset selectmode
  resetSelect(): void {
    this.selectedCount = 0;
    for (var i = 0; i < this.forms.length; i++) {
      this.selected[i] = false;
    }
    this.selectedItems = [];
    this.selectedIndices = [];
  }

  // UI feature to enable multiple deleting etc.
  selectItem(index: number) {
    if(this.selectMode){
      if(this.selectedItems.length > 0 && this.selectedItems.includes(this.forms[index])){
        this.selectMode ? this.selectedCount -= 1 : this.selectedCount = this.selectedCount;
        var empIndex = this.selectedItems.indexOf(this.forms[index], 0);
        if (empIndex > -1) {
          this.selectedItems.splice(empIndex, 1);
          this.selectedIndices.splice(index, 1);
          this.selected[index] = false;
        }
      }else{
        this.selectMode ? this.selectedCount += 1 : this.selectedCount = this.selectedCount;
        this.selectedItems.push(this.forms[index]);
        this.selectedIndices.push(index);
        this.selected[index] = true;
      }
    }
  }

  ngOnInit() {
    this.formButtonTooltips = ["Details", "Edit", "Delete", "Logs"];
    this.selected = [];
    this.selectedItems = [];
    this.selectMode = false;
    this.selectedCount = 0;
    this.selectedIndices = [];
    this.getForms();

  }


}
