/*
    create-form.component.ts - Create Form Component
    ---------------------
    The 'Create Form' Component is where the user will navigate to to create a new form 
    and contains the form builder UI which will change the currently displayed component
    based on selection

    (1) creates an instance of the 'Form' object
    (2) used for changing the view and pushing the field input added
    into the array of inputFields and the Form
    (3) function used for rendering a new form, giving it a date created/updated
    and posts the form to the database using the API
    (4) Switch statement for changing the view based off selection made in the mini
    toolbar - when one of the statements are true the view associated with it is rendered
    (5) this function is called when the page is opened. First, validators are added to the form
    so labels MUST be entered. Then, objects for templating the buttons and the button toolbar are created
*/

import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Form } from '../../../../Models/form';
import { InputField } from '../../../../Models/inputField';
import { APIService } from '../../../../Services/api.service';
import { LoginService } from '../../../../Services/login.service';

@Component({
  selector: 'app-create-form',
  templateUrl: './create-form.component.html',
  styleUrls: ['./create-form.component.css']
})
export class CreateFormComponent implements OnInit {

  constructor(private apiservice: APIService, private loginservice: LoginService, private route: ActivatedRoute, private router: Router) { }

  messageFromServer: boolean;
  label: string;
  message: string;
  renderedForm;
  buttons: any;
  buttonToolbar: any;
  enable: boolean[];
  clickedField: boolean;
  inputFields = [];
  currentDate: Date = new Date();

  // (1)
  newForm: Form = {
    label: undefined,
    createdBy: undefined,
    company: undefined,
    _id: undefined,
    _rev: undefined,
    dateCreated: undefined,
    dateUpdated: undefined,
    content: [],
    dynamicObject: undefined
  };

  // (2)
  fieldInputType(inputObj: any) {

    var inputField: InputField = new InputField();

    inputField.label = inputObj.label
    inputField.inputType = inputObj.inputType
    inputField.placeholder = inputObj.placeholder
    inputField.fieldInput = inputObj.fieldInput

    this.changeView(-1);
    this.newForm.content.push(inputField)
    this.inputFields.push(inputField)
    console.log(this.inputFields)

  }
 // (3)
  renderForm() {

    console.log("render form")

    this.newForm.label = this.label;
    this.newForm.company = 0;
    this.newForm.createdBy = JSON.parse(this.loginservice.getToken()).user._id;
    this.newForm.dateCreated = new Date();
    this.newForm.dateUpdated = new Date();
    
    if(confirm("Once a form is created you will not be able to edit it.\n\n\nDo you wish to continue?")) {
      this.apiservice.postForm(this.newForm)
      .subscribe(
        (response: any) => {
          this.messageFromServer = true;
          this.message = response.message;

          setTimeout(() => {
            this.router.navigate(['/manage-forms']);
          }, 50);
        },
        (err: any) => {
          console.log(err);
        });
    }

  }
  // (4)
  changeView(param) {

    switch (param) {

      case 0:
        this.enable = [true, false, false, false, false, false]
        this.clickedField = true;
        break;

      case 1:
        this.enable = [false, true, false, false, false, false]
        this.clickedField = true;

        break;

      case 2:
        this.enable = [false, false, true, false, false, false]
        this.clickedField = true;
        break;

      case 3:
        this.enable = [false, false, false, true, false, false]
        this.clickedField = true;
        break;

      case 4:
        this.enable = [false, false, false, false, true, false]
        this.clickedField = true;
        break;

      case 5:
        this.enable = [false, false, false, false, false, true]
        this.clickedField = true;
        break;

      case 6:
        this.enable = [false, false, false, false, false, false];
        break;

      case 7:
        this.enable = [false, false, false, false, false, false];
        break;

      default:
        this.enable = [false, false, false, false, false, false]
        this.clickedField = false;
        break;
    }
  }

  // (5)
  ngOnInit() {

    this.renderedForm = new FormGroup({
      label: new FormControl("", Validators.required)
    });


    this.enable = [false, false, false, false, false, false]


    this.buttons = [
      {
        name: "Input",
        icon: "fa fa-bars fa-5x",
        id: "input-box",
        pos: 0
      },
      {
        name: "Text Area",
        icon: "fa fa-file-text-o fa-5x",
        id: "text-area",
        pos: 1
      },
      {
        name: "Date",
        icon: "fa fa-calendar fa-5x",
        id: "date-picker",
        pos: 2
      },
      {
        name: "Checkbox",
        icon: "fa fa-check-square-o fa-5x",
        id: "check-box",
        pos: 3
      },
      {
        name: "Radio Button",
        icon: "fa fa-circle-o fa-5x",
        id: "radio-buttons",
        pos: 4
      },
      {
        name: "Dropdown",
        icon: "fa fa-caret-square-o-down fa-5x",
        id: "dropdown-list",
        pos: 5
      }
    ]


    this.buttonToolbar = [

      {
        icon: "fa fa-arrow-left",
        id: "back",
        enable: "!clickedField",
      },
      {
        icon: "fa fa-bars",
        id: "input",
        enable: "clickedField",
        pos: 0
      },
      {
        icon: "fa fa-file-text-o",
        id: "text-area",
        pos: 1
      },
      {
        icon: "fa fa-calendar",
        id: "date",
        enable: "clickedField",
        pos: 2
      },
      {
        icon: "fa fa-check-square-o",
        id: "check-box",
        enable: "clickedField",
        pos: 3

      },
      {
        icon: "fa fa-circle-o",
        id: "radio-button",
        enable: "clickedField",
        pos: 4
      },
      {
        icon: "fa fa-caret-square-o-down",
        id: "drop-down",
        enable: "clickedField",
        pos: 5
      }
    ]
  }
}
