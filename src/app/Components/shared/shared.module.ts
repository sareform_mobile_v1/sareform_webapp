/*
    shared.module.ts - Shared Module
    ---------------------
    Shared modules imports different modules and allows them to be used
    anywhere in the project that they are imported

    (1) imports the modules to be used for animations and design - ref: https://www.youtube.com/watch?v=-I1dT1huWdo&feature=youtu.be
    (2) exports the modules so they can be used anywhere 'Shared Module' is imported - ref: https://www.youtube.com/watch?v=-I1dT1huWdo&feature=youtu.be

*/
import { NgModule } from '@angular/core';
import { MatCardModule, MatNativeDateModule  } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatButtonModule} from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import {MatDatepickerModule} from '@angular/material/datepicker';


@NgModule({

    // (1)
    imports:[
        MatCardModule,
        MatInputModule,
        MatSidenavModule,
        MatButtonModule,
        MatListModule,
        MatDatepickerModule,
        MatNativeDateModule
    ],
    // (2)
    exports: [
        MatCardModule,
        MatInputModule,
        MatSidenavModule,
        MatButtonModule,
        MatListModule,
        MatDatepickerModule ,
        MatNativeDateModule
    ]
})

export class SharedModule {
    
}