/*
    core.module.ts - Core Module
    ---------------------
    The core module is for adding 'BrowserAnimationsModule' to the project
    allowing the module to be used anywhere it's imported

    (1) imports the module 'BrowserAnimationsModule' - ref: https://www.youtube.com/watch?v=-I1dT1huWdo&feature=youtu.be
*/

import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({

    imports: [BrowserAnimationsModule] // (1)
})
export class CoreModule {
    
}