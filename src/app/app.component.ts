import { Component, OnInit } from '@angular/core';
import { HomeComponent } from './Components/pages/home/home.component';
import { ActivatedRoute, Router } from '@angular/router';

import { LoginService } from './Services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  //title = 'app';
  //rootPage:any = HomeComponent;

  showHeader: boolean;

  constructor(private loginService: LoginService, private route: ActivatedRoute, private router: Router){}

  // When the main component initializes it checks if there is a cookie, if not it'll not show the navbar
  ngOnInit(): void{
    if(this.loginService.getIsAuthenticated()){
      this.showHeader = true;
    }else{
      this.showHeader = false;
    }
  }

}
